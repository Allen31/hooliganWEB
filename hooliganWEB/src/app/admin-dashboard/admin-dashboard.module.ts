import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import {SidenavComponent} from "./sidenav/sidenav.component";
import {UsersTableComponent} from "./users-table/users-table.component";
import {AdminRoutingModule} from "./admin-routing/admin-routing.module";
import {LibModule} from "../lib/lib.module";
import { ServersCardComponent } from './servers-card/servers-card.component';
import { InviteCodesTableComponent } from './inviteCodes-table/inviteCodes-table.component';

@NgModule({
  imports: [
    CommonModule,
    LibModule,
    AdminRoutingModule,
  ],
  exports: [
    SidenavComponent
  ],
  declarations: [
    HomeComponent,
    SidenavComponent,
    UsersTableComponent,
    ServersCardComponent,
    InviteCodesTableComponent,
  ]
})
export class AdminDashboardModule { }
