import { Component, OnInit } from '@angular/core';
import {ServersListService} from "../../services/servers-list.service";
import {ResData} from "../../models/resData";
import {MatSnackBar} from "@angular/material";
import {Server} from "../../models/server";

@Component({
  selector: 'app-servers-card',
  templateUrl: './servers-card.component.html',
  styleUrls: ['./servers-card.component.css']
})
export class ServersCardComponent implements OnInit {
  data: Server[];

  constructor(
    private dataService: ServersListService,
    public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.obtainData();
  }

  obtainData() : void {
    this.dataService.request().subscribe((resData: ResData) => {
      if (resData.result === 'success') {
        this.data = resData.message;
      } else {
        this.snackBar.open(resData.message, 'OK', {
          duration: 5000,
        })
      }
    }, (error) => {
      this.snackBar.open('An error occurred', 'OK', {
        duration: 5000,
      });
    });
  }
}
