import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServersCardComponent } from './servers-card.component';

describe('ServersCardComponent', () => {
  let component: ServersCardComponent;
  let fixture: ComponentFixture<ServersCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServersCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServersCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
