import { Component, OnInit } from '@angular/core';
import {BreakpointObserver, Breakpoints} from "@angular/cdk/layout";

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent {
  isHandset: boolean;

  constructor(breakPointObserver: BreakpointObserver) {
    breakPointObserver.observe([
      Breakpoints.Handset
    ]).subscribe((result) => {
      this.isHandset = result.matches;
    })
  }

}
