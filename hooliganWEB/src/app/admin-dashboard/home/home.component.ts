import { Component, OnInit } from '@angular/core';

export interface CardElement {
  cols: number;
  rows: number;
  title: string;
  contentTitle: string;
  contentValue: number;
  actions: string[];
  routerLink: string;
}

@Component({
  selector: 'app-admin-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {


  cards: CardElement[] = [
    {
      cols : 2,
      rows : 2,
      title : 'Users',
      contentTitle: 'Registered Numbers',
      contentValue: 45,
      actions : ['Check'],
      routerLink: '/users',
    }, {
      cols : 1,
      rows : 2,
      title : 'Servers',
      contentTitle: 'Available Servers',
      contentValue: 1,
      actions : ['Check'],
      routerLink: '/servers',
    }, {
      cols : 1,
      rows : 2,
      title : 'Invite Codes',
      contentTitle: 'Available Codes',
      contentValue: 10,
      actions : ['Check'],
      routerLink: '/inviteCodes',
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
