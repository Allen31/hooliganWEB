import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {UsersTableComponent} from "../users-table/users-table.component";
import {HomeComponent} from "../home/home.component";
import {ServersCardComponent} from "../servers-card/servers-card.component";
import {InviteCodesTableComponent} from "../inviteCodes-table/inviteCodes-table.component";

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'users', component: UsersTableComponent },
  { path: 'servers', component: ServersCardComponent },
  { path: 'inviteCodes', component: InviteCodesTableComponent },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule,
  ],
  declarations: []
})
export class AdminRoutingModule { }
