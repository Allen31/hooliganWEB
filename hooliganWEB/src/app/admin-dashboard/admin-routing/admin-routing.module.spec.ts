import { AdminRoutingModule } from './admin-routing.module';

describe('AdminRoutingModule', () => {
  let appRoutingModule: AdminRoutingModule;

  beforeEach(() => {
    appRoutingModule = new AdminRoutingModule();
  });

  it('should create an instance', () => {
    expect(appRoutingModule).toBeTruthy();
  });
});
