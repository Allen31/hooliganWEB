import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InviteCodesTableComponent } from './inviteCodes-table.component';

describe('InviteCodesTableComponent', () => {
  let component: InviteCodesTableComponent;
  let fixture: ComponentFixture<InviteCodesTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InviteCodesTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InviteCodesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
