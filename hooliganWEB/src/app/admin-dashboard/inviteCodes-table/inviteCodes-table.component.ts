import { Component, OnInit } from '@angular/core';
import {InviteCode} from "../../models/invite-code";
import {InviteCodesListService} from "../../services/inviteCodes-list.service";
import {MatSnackBar} from "@angular/material";

@Component({
  selector: 'app-invite-codes-table',
  templateUrl: './inviteCodes-table.component.html',
  styleUrls: ['./inviteCodes-table.component.css']
})
export class InviteCodesTableComponent implements OnInit {
  data: InviteCode[];
  columnTitles: string[] = [
    'id', 'code', 'invite_uid',
  ];

  constructor(private dataService: InviteCodesListService,
              public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.obtainData();
  }

  obtainData(): void {
    this.dataService.request().subscribe((resData) => {
      if (resData.result === 'success') {
        this.data = resData.message as InviteCode[];
      } else {
        this.snackBar.open(resData.message, 'OK', {
          duration: 5000,
        });
      }
    }, (err) => {
      this.snackBar.open('An error occurred', 'OK', {
        duration: 5000,
      });
    })
  }
}
