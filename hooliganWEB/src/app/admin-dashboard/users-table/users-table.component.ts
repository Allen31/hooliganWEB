import { Component, OnInit } from '@angular/core';
import {User} from "../../models/user";
import {ResData} from "../../models/resData";
import {UsersListService} from "../../services/users-list.service";
import {MatSnackBar} from "@angular/material";

@Component({
  selector: 'app-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.css']
})
export class UsersTableComponent implements OnInit {
  usersData: User[];
  columnTitles: string[] = [
    'uid', 'user_name', 'user_email',
    'service_port', 'service_password', 'service_traffic_total', 'service_traffic_used',
    'account_type', 'account_ref_uid', 'account_register_date'
  ];

  constructor(
    private usersListService: UsersListService,
    public snackBar: MatSnackBar) {  }

  ngOnInit() {
    this.obtainData();
  }

  obtainData(): void {
    this.usersListService.request()
      .subscribe( (resData: ResData) => {
        if (resData.result === 'success') {
          this.usersData = resData.message as User[];
        } else {
          // Show the failure message
          this.snackBar.open(resData.message, 'OK', {
            duration: 5000,
          });
        }
      }, (error) => {
        // TODO
        // Add 'reload' feature to actions.
        this.snackBar.open('An error occurred', 'OK', {
          duration: 5000,
        });
      });
  }

}
