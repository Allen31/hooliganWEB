export interface InviteCode {
  id: number;
  code: string;
  invite_uid: number;
}
