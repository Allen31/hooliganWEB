export interface Server {
  server_ip: string;
  server_location: string;
  config_method: string;
  config_protocol: string;
  config_protocol_param: string;
  config_obfs: string;
  config_obfs_param: string;
}
