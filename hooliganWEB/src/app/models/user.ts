export interface User {
  uid: number;
  user_name: string;
  user_email: string;
  service_port: number;
  service_password: string;
  service_traffic_total: number;
  service_traffic_used: number;
  account_type: string;
  account_ref_uid: number;
  account_register_date: string;
}
