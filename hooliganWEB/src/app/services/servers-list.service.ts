import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ResData} from "../models/resData";
import {Observable} from "rxjs/internal/Observable";
import {Server} from "../models/server";
import {of} from "rxjs/index";

@Injectable({
  providedIn: 'root'
})
export class ServersListService {
  private url = 'http://192.168.199.188:3000/admin/_serverstest';

  mockServers: Server[] = [
    {
      server_ip: 'ss.hooliganallen3.ml',
      server_location: 'HONG KONG',
      config_method: 'none',
      config_protocol: 'auth_chain_a',
      config_protocol_param: '3',
      config_obfs: 'http_simple',
      config_obfs_param: 'cn.bing.com',
    },
  ];

  constructor(private httpClient: HttpClient) { }

  request(): Observable<ResData> {
    //TODO
    // When an error occurred.

    // return this.httpClient.get<ResData>(this.url);
    /**
    * FOR TEST USE
    * */
    return of({
      result: 'success',
      message: this.mockServers,
    });
  }
}
