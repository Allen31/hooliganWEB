import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ResData} from "../models/resData";
import {Observable, of} from "rxjs/index";
import {InviteCode} from "../models/invite-code";

@Injectable({
  providedIn: 'root'
})
export class InviteCodesListService {
  private url = 'http://192.168.199.188:3000/admin/_inviteCodes';

  mockCodes : InviteCode[] = [
    {
      id: 1,
      code: '1fajsdifjialsjdfioovc',
      invite_uid: 1,
    },
    {
      id: 2,
      code: 'sdfjaosg798asd8f9asdf',
      invite_uid: 1,
    },
    {
      id: 3,
      code: '989sdfsjkdjfksdjfjsdf',
      invite_uid: 1,
    }
  ];

  constructor(private httpClient: HttpClient) { }

  request(): Observable<ResData> {
    // return this.httpClient.get<ResData>(this.url);
    return of({
      result: 'success',
      message: this.mockCodes,
    });
  }
}
