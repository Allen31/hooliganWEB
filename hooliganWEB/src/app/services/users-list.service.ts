import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/internal/Observable";
import {ResData} from "../models/resData";


@Injectable({
  providedIn: 'root'
})
export class UsersListService {
  private url = 'http://192.168.199.188:3000/admin/_userstest';

  constructor(
    private http: HttpClient,
  ) { }

  request(): Observable<ResData> {
    //TODO
    // When an error occurred.
    return this.http.get<ResData>(this.url);
  }
}
