import { TestBed, inject } from '@angular/core/testing';

import { InviteCodesListService } from './inviteCodes-list.service';

describe('InviteCodesListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InviteCodesListService]
    });
  });

  it('should be created', inject([InviteCodesListService], (service: InviteCodesListService) => {
    expect(service).toBeTruthy();
  }));
});
