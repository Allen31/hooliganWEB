import { TestBed, inject } from '@angular/core/testing';

import { ServersListService } from './servers-list.service';

describe('ServersListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServersListService]
    });
  });

  it('should be created', inject([ServersListService], (service: ServersListService) => {
    expect(service).toBeTruthy();
  }));
});
