import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule,
  MatCardModule,
  MatDividerModule,
  MatGridListModule, MatIconModule,
  MatListModule, MatSidenavModule, MatSnackBarModule, MatTableModule,
  MatToolbarModule
} from "@angular/material";
import {LayoutModule} from "@angular/cdk/layout";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  imports: [
    CommonModule,
    /* @Angular/material  */
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    MatGridListModule,
    MatCardModule,
    MatDividerModule,
    MatSnackBarModule,
    MatListModule,
    /* @Angular/cdk */
    LayoutModule,
    /* @Others */
    HttpClientModule,
  ],
  exports: [
    /* @Angular/material  */
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    MatGridListModule,
    MatCardModule,
    MatDividerModule,
    MatSnackBarModule,
    MatListModule,
    /* @Angular/cdk */
    LayoutModule,
    /* @Others */
    HttpClientModule,
  ],
  declarations: []
})
export class LibModule { }
